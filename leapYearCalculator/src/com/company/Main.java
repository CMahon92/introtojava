package com.company;

public class Main {

    public static void main(String[] args) {
	int year = 1925;
    isLeapYear(year);

    }

    public static boolean isLeapYear(int year) {
        if (year < 1 || year > 9999) {
            System.out.println("this is not a valid year");
            return false;

        }else if (year % 4 == 0 && year % 100 == 0) {
            boolean isLeapYear = year % 400 == 0;
            System.out.println(isLeapYear);
            return isLeapYear;

        }else if (year % 4 == 0) {
            System.out.println("this is a leap year");
            return true;
        }
        else {
            System.out.println("not a leap year");
            return false;
        }

    }
}
