package com.company;

public class Main {

    public static void main(String[] args) {
        int firstAge = 23;
        int secondAge = 3;
        int thirdAge = 29;
        hasTeen(firstAge,secondAge,thirdAge);
        isTeen(firstAge);

    }
    public static boolean hasTeen(int firstAge, int secondAge, int thirdAge){
        boolean teenagerOne = (firstAge  >=13 && firstAge <= 19)? true:false;
        boolean teenagerTwo = (secondAge  >=13 && secondAge <= 19)? true:false;
        boolean teenagerThree = (thirdAge  >=13 && thirdAge <= 19)? true:false;

        if(teenagerOne || teenagerTwo || teenagerThree == true) {
            System.out.println("There is a teenager present");
            return true;
        } else {
            System.out.println("There is NO teenager present");
            return false;
        }
    }

    public static boolean isTeen(int firstAge) {
        if (firstAge >= 13 && firstAge <=19) {
            System.out.println("true");
            return true;
        }else {
            System.out.println("false");
            return false;
        }
    }
}
