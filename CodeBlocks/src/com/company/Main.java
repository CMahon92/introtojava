package com.company;

public class Main {

    public static void main(String[] args) {

        //Variable created outside the code block
        boolean gameOver = true;
        int firstScore =900;
        int levelCompleted = 5;
        int bonus = 100;

        if (firstScore < 5000 && firstScore > 1000) {
            System.out.println("Your score is less than 5000 and more than 1000");
        } else if (firstScore < 1000) {
            System.out.println("Your score is less than 1000");
        } else {
            System.out.println("You didn't meet the criteria");
        }

        //Variable created within the code block
        if (gameOver ==true) {
            int finalScore = firstScore + (levelCompleted * bonus);
            System.out.println("Your final score was " + finalScore);
        }

        /*Challenge
            1. Print out a second Score on the screen with the following
            2. Score set to 10000
            3. levelCompleted set to 8
            4. bonus set to 200
            5. Make sure the first printout above stil displays*/

        //My Solution
            int secondScore = 10000;
            int secondLevelCompleted = 8;
            int secondBonus = 200;

        if (gameOver ==true) {
            int finalScore = secondScore + (secondLevelCompleted * secondBonus);
            System.out.println("Your final score was " + finalScore);
        }

        //Solution from instructor:
        // Advantage - Quicker to do and uses memory more efficiently
        // Disadvantage - Don't have permanent record of previous variables and copying pasting code
//        firstScore =10000;
//        levelCompleted = 8;
//        bonus = 200;
//
//        if (gameOver) {
//            int finalScore = firstScore + (levelCompleted * bonus);
//            System.out.println("Your final score was " + finalScore);
//        }


    }
}
