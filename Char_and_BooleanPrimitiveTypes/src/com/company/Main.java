package com.company;

public class Main {

    public static void main(String[] args) {

        //char primitive type
        char myChar = 'D';
        char myUnicodeChar = '\u0044';
        System.out.println(myChar);
        System.out.println(myUnicodeChar);
        char myCopyrightChar = '\u00A9';
        System.out.println(myCopyrightChar);

        //boolean primitive type
        boolean trueBooleanValue = true;
        boolean falseBooleanValue = false;

        boolean isCustomerOverTwentyOne = true;

    }
}
