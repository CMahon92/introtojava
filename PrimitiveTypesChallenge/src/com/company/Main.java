package com.company;

public class Main {

    public static void main(String[] args) {
	// write your code here

    /* Challenge is to create a byte variable and set it to any valid byte number.
       Create a short variable and set it to any valid short number.
       Create an int variable and set it to any valid int number. Lastly, create a variable
       of type long and make it equal to 50000 plus 10 times the sum of the byte plus the
       short plus the integer values.*/

        byte newByteVar = 122;
        short newShortVar = 3000;
        int newIntVar = 43000;
        long newLongVar = 50000L + (10*(newByteVar + newShortVar + newIntVar));
        System.out.println(newLongVar);

    }
}
