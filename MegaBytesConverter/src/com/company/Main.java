package com.company;

public class Main {

    public static void main(String[] args) {
//        1. Write a method called printMegaBytesAndKiloBytes that has 1 parameter of type int with the name kiloBytes.
//        2. The method should not return anything (void) and it needs to calculate the megabytes and remaining
//           kilobytes from the kilobytes parameter.
//        3. Then it needs to print a message in the format "XX KB = YY MB and ZZ KB".
//                XX represents the original value kiloBytes.
//                YY represents the calculated megabytes.
//                ZZ represents the calculated remaining kilobytes.
//        4. If the parameter kiloBytes is less than 0 then print the text "Invalid Value".
//
//         TIP: Use the remainder operator
//         TIP: 1 MB = 1024 KB
//
        int kiloBytes = 5000;
        printMegaBytesAndKiloBytes(kiloBytes);
    }

    public static void printMegaBytesAndKiloBytes(int kiloBytes) {
        if(kiloBytes < 0) {
            System.out.println("Invalid Value");
        }else {
            int megaBytes = kiloBytes / 1024;
            int kiloBytesRemainder = kiloBytes % 1024;
            System.out.println(kiloBytes + " KB = " + megaBytes + " MB and " + kiloBytesRemainder + " KB");
        }
    }
}
