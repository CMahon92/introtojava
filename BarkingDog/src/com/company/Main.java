package com.company;

public class Main {

    public static void main(String[] args) {

//        We have a dog that likes to bark.  We need to wake up if the dog is barking at night!
//          1. Write a method shouldWakeUp that has 2 parameters.
//             - 1st parameter should be of type boolean and be named barking it represents if our dog is currently barking.
//             - 2nd parameter represents the hour of the day and is of type int with the name hourOfDay and has a valid range of 0-23.
//          2. We have to wake up if the dog is barking before 8 or after 22 hours so in that case return true.
//             In all other cases return false.
//           3. If the hourOfDay parameter is less than 0 or greater than 23 return false.

        //I added the print lines within the shouldWakeUp method so the logic can be visually seen working
        boolean barking = true;
        int hourOfDay = 6;
        shouldWakeUp(barking,hourOfDay);

    }

    public static boolean shouldWakeUp(boolean barking, int hourOfDay) {
        if(hourOfDay < 0 || hourOfDay > 23) {
            System.out.println("Invalid Value");
            return false;
        }else if(hourOfDay < 8 || hourOfDay > 22) {
            System.out.println("Dog is barking");
            return barking;
        } else {
            System.out.println("Stay Asleep");
            return false;
        }

    }
}
