package com.company;

import java.math.RoundingMode;
import java.text.DecimalFormat;

public class Main {

    public static void main(String[] args) {


        double firstDoubleValue = 3.0;
        double secondDoubleValue = -3.0;

        areEqualByThreeDecimalPlaces(firstDoubleValue,secondDoubleValue);
    }

    public static boolean areEqualByThreeDecimalPlaces(double firstDoubleValue,double secondDoubleValue ) {
        DecimalFormat df = new DecimalFormat("0.000");
        df.setRoundingMode(RoundingMode.DOWN);
        String formattedFirstString = df.format(firstDoubleValue);
        double formattedFirstDouble = Double.parseDouble(formattedFirstString);

        String formattedSecondString = df.format(secondDoubleValue);
        double formattedSecondDouble = Double.parseDouble(formattedSecondString);

        if(formattedFirstDouble == formattedSecondDouble) {
            System.out.println("true");
            return true;
        } else {
            System.out.println(false);
            return false;
        }
    }
}
