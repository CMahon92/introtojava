public class Hello {

    public static void main(String[] args) {
        System.out.println("Hello World");

        int firstEquation = (9 * 4) - (4 + 3) ;
        System.out.println("August 2021, I turned " + firstEquation + " years old.");

        int mySecondNumber = 100;
        int myThirdNumber = 17;
        int total = mySecondNumber + myThirdNumber;
        System.out.println("My Total is " + total);

        int myLastOne = 1000 - total;
        System.out.println("My last equation equals " + myLastOne);
    }
}
