package com.company;

public class Main {

    public static void main(String[] args) {

       calcFeetAndInchesToCentimeters(6.00d,9.00d);
        calcFeetAndInchesToCentimeters(157.00d);
    }

    public static double calcFeetAndInchesToCentimeters(double feet, double inches) {
        boolean feetNum = (feet >= 0) ? true : false;
        boolean inchesNum = (inches >= 0 && inches <= 12) ? true : false;
        if (feetNum ==  true && inchesNum == true) {
            double centimeters = (feet * 12) * 2.54;
            centimeters += inches * 2.54;
            System.out.println(feet + " feet, " + inches + " inches = " + centimeters + " cm");
            return centimeters;
        } else {
            System.out.println("-1");
            return -1;
        }
    }

    public static double calcFeetAndInchesToCentimeters(double inches) {
        if (inches < 0) {
            System.out.println("-1");
            return -1;
        }
        double feet = (int) inches /12;
        double remainingInches = (int) inches % 12;
        System.out.println(inches + " inches is equal to " + feet + " feet and " + remainingInches + " inches");
        return calcFeetAndInchesToCentimeters(feet,remainingInches);
    }
    }

