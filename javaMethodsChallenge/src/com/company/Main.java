package com.company;

public class Main {

    public static void main(String[] args) {

           /* Methods Challenge
    1. Create a method called displayHighScorePosition
    2. It should display a players name as a parameter, and a 2nd parameter as a position in the high
       score table.
    3. You should display the players name along with a message like "managed to get into
       position " and the position they got and a further message " on the high score table."

    4. Create a 2nd method called calculateHighScorePosition
    5. It should be sent one argument only, the player score
    6. It should return an int
    7. The return date should be:
        - 1 if the score is >= 1000
        - 2 if the score is >= 500 and < 1000
        - 3 if the score is >= 100 and < 500
        - 4 in all other cases
    8. Call both methods and display the results of the following:
        - a score of 1500, 900, 400, and 50*/

        String playerNameOne = "Leena";
        String playerNameTwo = "Olivia";
        String playerNameThree = "Aaron";
        String playerNameFour = "Stormy";
        int playerScore = 1500;
        int position = calculateHighScorePosition(playerScore);
        displayHighScorePosition(playerNameOne, position);

        playerScore = 900;
        position = calculateHighScorePosition(playerScore);
        displayHighScorePosition(playerNameTwo, position);

        playerScore = 400;
        position = calculateHighScorePosition(playerScore);
        displayHighScorePosition(playerNameThree, position);

        playerScore = 50;
        position = calculateHighScorePosition(playerScore);
        displayHighScorePosition(playerNameFour, position);
    }

        public static void displayHighScorePosition(String playerName,int position) {

            System.out.println(playerName + ", you managed to get into position " + position +" on the high score table.");

        }
        public static int calculateHighScorePosition(int playerScore ) {
            if(playerScore >= 1000) {
                return 1;
            }else if (playerScore >= 500 && playerScore < 1000) {
                return 2;
            }else if (playerScore >= 100 && playerScore < 500) {
                return 3;
            }else {
                return 4;
            }
        }

}
