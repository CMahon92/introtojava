package com.company;

public class Main {

    public static void main(String[] args) {
	// write your code here
    
        float myMinFloatValue = Float.MIN_VALUE;
        float myMaxFloatValue = Float.MAX_VALUE;
        System.out.println("My minimum float value = " + myMinFloatValue);
        System.out.println("My maximum float value = " + myMaxFloatValue);

        double myMinDoubleValue = Double.MIN_VALUE;
        double myMaxDoubleValue = Double.MAX_VALUE;
        System.out.println("My minimum double value = " + myMinDoubleValue);
        System.out.println("My maximum double value = " + myMaxDoubleValue);

        //float myFloatValue = 5.25 Error Message: "Required Type: Float Provided: Double"
        float myFloatValue = 5.25f;
        double myDoubleValue = 6.45d;
        System.out.println("This is a float value = " + myFloatValue);
        System.out.println("This is a double value = " + myDoubleValue);

        //Floating Point Precision

        int myIntValue = 5/3;
        float mySecFloatValue = 5f / 3f;
        double mySecDoubleValue = 5d / 3d;
        System.out.println("My int value of 5/3 = " + myIntValue);
        System.out.println("My float value of 5f/3f = " + mySecFloatValue);
        System.out.println("My double value of 5d/3d= " + mySecDoubleValue);

        /*Challenge : Convert a given number of pounds to kilograns
            Steps:
                1. Create a variable with the appropriate type to store the number of pounds to be
                   converted to kilograms.
                2. Calcualte the result i.e. the number of kilograms based on the contents of the
                   variable above and store the result in 2nd appropriate variable.
                3. Print out the result
                 *****Note: 1 pound is equal to 0.45359237 of a kilogram                        */

        int pounds = 6;
        double kilograms = 0.45359237d;
        double conversion = pounds * kilograms;
        System.out.println("Conversion of pounds to kgs = " + conversion);

        //Learned although above solution is correct, I should use the same data type
        //Code could of also been broken down into 3 lines of code instead of 4
        //Below is the same result using 3 lines of code and same data type

        double lbs = 6d;
        double kgsConversion = 0.45359237d * lbs;
        System.out.println("Conversion of pounds to kgs redone = " + kgsConversion);
    
    }
}
