package com.company;

public class Main {

    public static void main(String[] args) {

        int myMinIntValue = Integer.MIN_VALUE;
        int myMaxIntValue = Integer.MAX_VALUE;
        System.out.println("Integer Minimum Value = " + myMinIntValue );
        System.out.println("Integer Maximum Value = " + myMaxIntValue );

        System.out.println("Busted Max Value = " + (myMaxIntValue + 1));
        System.out.println("Busted Min Value = " + (myMinIntValue - 1));

        byte myMinByteValue = Byte.MIN_VALUE;
        byte myMaxByteValue = Byte.MAX_VALUE;
        System.out.println("Byte Minimum Value = " + myMinByteValue);
        System.out.println("Byte Maximum Value = " + myMaxByteValue);

        short myMinShortValue = Short.MIN_VALUE;
        short myMaxShortValue = Short.MAX_VALUE;
        System.out.println("Short Minimum Value = " + myMinShortValue);
        System.out.println("Short Maximum Value = " + myMaxShortValue);

        long myLongValue = 100L;
        long myMinLongValue = Long.MIN_VALUE;
        long myMaxLongValue = Long.MAX_VALUE;
        System.out.println("Long Minimum Value = " + myMinLongValue);
        System.out.println("Long Maximum Value = " + myMaxLongValue);

        //long LiteralLongValue = 2147483647234; -Error message "integer to large"
        long LiteralLongValue = 2147483647234L;
        System.out.println("This is a literal long value = " + LiteralLongValue);

        //Learning Casting in Java
        //byte myNewByteValue = (myMinByteValue/2); Error Message "incompatible types. Required: byte Found: int
        byte myNewByteValue = (byte)(myMinByteValue/2);
        System.out.println("My new byte value = " + myNewByteValue);

        //short myNewShortValue = (myMinShortValue/2); Error Message "incompatible types. Required: short Found: int
        short myNewShortValue = (short) (myMinShortValue / 2);
        System.out.println("My new short value = " + myNewShortValue);

    }
}
