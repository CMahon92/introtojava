package com.company;

public class Main {

    public static void main(String[] args) {

        int result = 1 + 2; //1+2 = 3
        System.out.println("1 + 2 = " + result);

        int previousResult = result; // 3
        System.out.println("Previous Result = " + previousResult);

        result = result - 1; //3 - 1 = 2
        System.out.println("3 - 1 = " + result);
        System.out.println("Previous Result = " + previousResult);

        result = result * 10; //2 * 10 = 20
        System.out.println("2 * 10 = " + result);

        result = result / 5; // 20 / 5 = 4
        System.out.println("20 / 5 = " + result);

        //Module Operator
        result = result % 3; //remainder of (4 % 3) = 1
        System.out.println("4 / 3 = " + result);

        //Abbreviating Operators
        //result = result + 1
        result++; // 1 + 1 = 2
        System.out.println("1 + 1 = " + result);

        //result = result - 1
        result--; //2 - 1 = 1
        System.out.println("2 - 1 = " + result);

        //result = result + 2
        result += 2; //1 + 2 = 3
        System.out.println("Addition: 1 + 2 = " + result);

        //result = result * 10
        result *= 10; //3 * 10 = 30
        System.out.println("Multiplication: 3 * 10 = " + result);

        //result = result / 3
        result /= 3; // 30 / 3 = 10
        System.out.println("Division: 30 / 3 = " + result);

        //result = result - 2
        result -= 2; // 10 - 2 = 8
        System.out.println("Subtraction: 10 - 2 = " + result);

        //if - then statement
        boolean isAlien = false;
        if (isAlien == false) {
            System.out.println("It is not an alien!");
            System.out.println("I am also afraid of Aliens!");
        }

        //Logical && Operator
        int topScore = 80;
        if (topScore < 100) {
            System.out.println("You got the high score!");
        }

        int secondTopScore = 75;
        if (topScore > secondTopScore && topScore < 100) {
            System.out.println("Top score is greater than second top score and less than 100");
        }

        //Logical || Operator
        if ((topScore > 90) || (secondTopScore <= 90)) {
            System.out.println("Either or both of the conditions are true");
        }

        //Assignemnt Operator vs Equals to Operator
        //Error Message: Incompatible types. Required: boolean, Found: int
        /* int newValue = 50;
            if (newValue = 50) {
                System.out.println("This an error");
         }                                              */

        int newValue = 50;
        if (newValue == 50) {
            System.out.println("This true");
        }

        boolean isCar = false;
        if (!isCar) {
            System.out.println("This should print");
        }

        //Ternary Operator
        int ageOfClient = 29;
        boolean isOverEighteen = (ageOfClient >= 20)? true : false;
        if (isOverEighteen) {
            System.out.println("You are over 18. You can come in!");
        }

        //Challenge
        /* 1. Create a double variable with the value of 20.00
           2. Create a second variable type double with the value of 80.00
           3. Add both numbers together and multiply by 100
           4. Use the remainder operator to figure out what the remainder from the result of the
              operation in step 3 and 40.00
           5. Create a boolean variable that assigns the value true if the remainder in #4 is 0,
              or false if it's not zero.
            6. Output the boolean variable
            7. Write an if-then statement that displays a message "Got some remainder" if the boolean
               in step 5 is not true. */

        double firstDoubleVar = 20.00;
        double secondDoubleVar = 80.00;
        double resultDoubleVar = (firstDoubleVar + secondDoubleVar) * 100d;
        double remainder = resultDoubleVar % 40d;
        boolean equalToZero = (remainder == 0.0)? true : false;
        System.out.println(equalToZero);
        if (equalToZero == false) {
            System.out.println("Got some remainder");
        }

        //Reiterated challenge w/o parentheses for false message to print
        double firstDoubleValue = 20.00;
        double secondDoubleValue = 80.00;
        double resultDoubleValue = firstDoubleValue + secondDoubleValue * 100d;
        double isRemainder = resultDoubleValue % 40d;
        boolean isZero = (isRemainder == 0.0)? true : false;
        System.out.println(isZero);
        if (isZero == false) {
            System.out.println("Got some remainder");
        }
    }
}
