package com.company;

public class Main {

    public static void main(String[] args) {

        //String Data Type
        String myString = "- This is a string";
        System.out.println("My string is equal to " + myString);

        myString = myString + ", and this string as well";
        System.out.println("My string is equal to " + myString);

        myString = myString + " \u00A9 2021";
        System.out.println("My string is equal to " + myString);

        //Number String
        String numberString = "200.50";
        numberString = numberString + "49.50";
        System.out.println(numberString);

        String lastNumString = "10";
        int myInt = 40;
        lastNumString = lastNumString + myInt;
        System.out.println("Last number string = " + lastNumString);

        double doubleNum = 128.43d;
        lastNumString = lastNumString + doubleNum;
        System.out.println("Last number string = " + lastNumString);



    }
}
